const express = require('express');
const cors = require('cors');

class Server {

    constructor() {
        this.userRoutes = '/api/users'        
        this.PORT = process.env.PORT

        this.app = express()

        this.middlewares();

        this.routes();

    }

    routes() {

        this.app.use(this.userRoutes , require('../routes/user'))

    }

    middlewares() {
        this.app.use(express.static('public'))
        this.app.use(cors())
        this.app.use(express.json())
    }

    listen() {

        this.app.listen(this.PORT, () => {

            console.log(`listen in port ${this.PORT}`)

        });
    }

}

module.exports = Server