const {Router} = require('express');
const { GetUsers, PostUser, PutUser } = require('../controllers/user.controller');

const router = Router()

router.get('/', GetUsers)

router.post('/', PostUser)

router.put('/:id', PutUser)

router.delete('/', (req, res) => {

    res.json({ msg: 'rest-server-delete' })

})



module.exports = router