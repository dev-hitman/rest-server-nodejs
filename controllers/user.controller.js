const { response, request } = require('express')


const GetUsers = (req = request, res = response) => {

    const {lastname , q} = req.query;


    res.json(
        {
            msg: 'rest-server-get-controller',
            lastname,
            q
        }
    )

}

const PostUser = (req = request, res = response) => {

    const body = req.body;

    res.json({
        msg: 'rest-server-post',
        body
    })

}

const PutUser = (req = request, res = response) => {

    const id = req.params.id

    res.json({
        msg: 'rest-server-put-controller',
        id
    }
    )

}

module.exports = { GetUsers, PostUser, PutUser }